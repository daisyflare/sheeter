use crate::Position;

use std::error;
use std::fmt;

#[derive(Debug)]
pub enum ParseError {
    InvalidEquation(Position),
    RefLoop(Position, Position)
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            ParseError::InvalidEquation(pos) => write!(f, "invalid equation in cell {pos}"),
            ParseError::RefLoop(pos1, pos2) => write!(f, "reference loop: {pos1} refers to {pos2}")
        }
    }
}

impl error::Error for ParseError {
}

#[derive(Debug)]
pub enum RunTimeError {}

impl fmt::Display for RunTimeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            _ => todo!()
        }
    }
}

impl error::Error for RunTimeError {

}
