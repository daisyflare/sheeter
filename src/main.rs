use std::error::Error;
use std::iter;
use std::fmt;
use std::fmt::Write;
use std::string::String;
use std::ops;
use std::collections::HashSet;

mod errors;
use crate::errors::*;

#[derive(Debug)]
enum Cell {
    Num(isize),
    Text(String),
    Eq(Equation),
    Nothing
}
use Cell::*;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
struct Position {
    col: usize,
    row: usize
}

impl Position {
    fn new(col: usize, row: usize) -> Self {
        Position { col, row }
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let mut max_power = 0;
        loop {
            if 26usize.pow(max_power) > self.col {
                break;
            }
            max_power += 1;
        }
        let mut to_push = self.col + 1; // +1 because it starts at one
        max_power = max_power.max(1);
        while max_power > 0 {
            let base = 26_usize.pow(max_power - 1);
            let p = (to_push / base) as u32;
            let c = char::from_digit(p - 1 + 10, 36).unwrap();
            f.write_char(c.to_ascii_uppercase())?; // minus one because it starts at 0
            to_push -= (p as usize) * base;
            max_power -= 1;
        }
        write!(f, "{}", self.row + 1)
    }
}

#[derive(Debug)]
enum EquationTree {
    Equals(Position)
}

#[derive(Debug)]
struct Equation {
    tree: EquationTree,
    depends: Option<HashSet<Position>>,
    final_value: Option<Box<Cell>>
}

impl Cell {
    fn render(&self, sheet: &Sheet) -> String {
        match self {
            Num(val)  => val.to_string(),
            Text(s)   => s.clone(),
            Eq(eq)    => eq.render(sheet),
            Nothing   => String::new()
        }
    }

    fn is_equation(&self) -> bool {
        if let Eq(_) = &self {
            true
        } else {
            false
        }
    }
    fn unwrap_eq_ref(&self) -> &Equation {
        if let Eq(e) = &self {
            return e
        } else {
            panic!()
        }
    }
    fn unwrap_eq_mut(&mut self) -> &mut Equation {
        if let Eq(e) = self {
            return e
        } else {
            panic!()
        }
    }
    fn clone_non_eq(&self) -> Self {
        match self {
            Text(s) => Text(s.clone()),
            Num(n) => Num(*n),
            Nothing => Nothing,
            Eq(_) => panic!("Equations cannot be cloned!!")
        }
    }
}

impl Equation {
    fn render(&self, sheet: &Sheet) -> String {
        if let Some(c) = &self.final_value {
            c.render(sheet)
        } else {
            match &self.tree {
                EquationTree::Equals(pos) => format!("{{=value of cell at position {:?}}}", (pos.col + 1, pos.row + 1))
            }
        }
    }

    fn new(tree: EquationTree) -> Self {
        Equation {
            tree,
            depends: None,
            final_value: None
        }
    }
}

#[derive(Debug)]
struct Sheet {
    rows: usize,
    cols: usize,
    data: Vec<(Position, Cell)>,
}

impl ops::Index<Position> for Sheet {
    type Output = Cell;

    fn index(&self, index: Position) -> &Cell {
        &self.data[index.row * self.rows + index.col].1
    }
}

impl ops::IndexMut<Position> for Sheet {

    fn index_mut(&mut self, index: Position) -> &mut Cell {
        &mut self.data[index.row * self.rows + index.col].1
    }
}

impl fmt::Display for Sheet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        for (pos, item) in &self.data {

            if pos.col != 0 {
                write!(f, " | ")?;
            } else {
                if pos.row != 0 {
                    write!(f, "\n")?;
                }
                write!(f, " ")?;
            }
            let cell_display = item.render(&self);
            write!(f, "{cell_display}")?;
        }
        Ok(())
    }
}

impl Sheet {
    fn from_str(input: &str) -> Sheet {
        let mut max_len = 0;

        // make list of rows, each list of cols in form
        // ((col_number, data), row_number)
        let mut by_rows = input.lines()
                               .map(str::trim)
                               .enumerate()
                               .map(|(n,s)| s.split('|')
                                    .map(str::trim)
                                    .enumerate()
                                    .zip(iter::repeat(n))
                                    .collect::<Vec<_>>())
                               .collect::<Vec<_>>();

        // Make sure all rows have the same number of columns by adding
        // empty ones at the end
        for row in &by_rows {
            max_len = max_len.max(row.len());
        }
        let mut row_count = 0;
        for row in &mut by_rows {
            let mut col_count = row.len();
            if row.len() < max_len {
                for _ in 0..(max_len - row.len()) {
                    row.push(((col_count, ""), row_count));
                    col_count += 1;
                }
            }
            row_count += 1;
        }

        for row in &by_rows {
            assert!(row.len() == max_len);
        }

        let parsed_cells = by_rows.into_iter()
                                  .flatten()
                                  .map(|((col, item), row)|(Position::new(col, row),
                                                            parse_cell(item, Position::new(col, row)).unwrap()))
                                  .collect::<Vec<_>>();

        let sheet = Sheet {
            data: parsed_cells,
            rows: row_count,
            cols: max_len,
        };

        // sheet.verify();

        sheet
    }

    // TODO: have equations treat out-of-bounds locations as just
    // empty spaces
    fn check_pos_in_bounds(&self, pos: Position) {
        if pos.row >= self.rows || pos.col >= self.cols {
            unimplemented!()
        }
    }

    fn solve(&mut self) -> Result<(), RunTimeError> {
        for i in 0..(self.data.len()) {
            let pos = self.data[i].0;
            if self.data[i].1.is_equation() {
                let c = self.resolve(pos, true)?;
                assert!(!c.is_equation());
            }
        }
        Ok(())
    }

    fn resolve(&mut self, pos: Position, useful: bool) -> Result<Cell, RunTimeError> {
        if let Eq(e) = &self[pos] {
            if let Some(s) = &e.final_value {
                if useful {
                    return Ok(s.clone_non_eq());
                } else {
                    return Ok(Nothing)
                }
            }
            let dependencies = e.depends.as_ref()
                                        .expect("resolve should only be used after verify!")
                                        .iter()
                                        .copied()
                                        .collect::<Vec<_>>();

            for dependency in dependencies {
                self.resolve(dependency, false)?;
            }
        } else {
            if useful {
                return Ok(self[pos].clone_non_eq())
            } else {
                return Ok(Nothing)
            }
        }
        let s = match self[pos].unwrap_eq_ref().tree {
            EquationTree::Equals(lower_pos) => {
                self[pos].unwrap_eq_mut().final_value = Some(Box::new(self.resolve(lower_pos, true)?));
                self.resolve(lower_pos, true)?
            }
        };
        Ok(s)
    }

    fn populate_dependencies(&mut self, pos: Position, highest_pos: Position) -> Result<(), ParseError> {
        if self[pos].unwrap_eq_ref().depends.is_some() {
            return Ok(())
        }

        // Mini-function that gets repeated throughout the arms of the match tree
        fn is_eq_repeat(s: &mut Sheet, dependencies: &mut HashSet<Position>, pos: Position, h_p: Position) -> Result<(), ParseError> {
            if s[pos].is_equation() {
                s.populate_dependencies(pos, h_p)?;
                dependencies.extend(s[pos]
                                    .unwrap_eq_ref()
                                    .depends.as_ref()
                                    .expect("Should be filled by populating dependencies!")
                                    .iter());
            }
            Ok(())
        }
        let mut dependencies = HashSet::new();
        let Eq(e) = &self[pos] else { unreachable!() };
        match e.tree {
            EquationTree::Equals(pos) => {
                self.check_pos_in_bounds(pos);
                dependencies.insert(pos);
                if dependencies.contains(&highest_pos) {
                    return Err(ParseError::RefLoop(highest_pos, pos));
                }
                is_eq_repeat(self, &mut dependencies, pos, highest_pos)?;

            }
        }
        let Eq(ref mut e) = &mut self[pos] else { unreachable!() };
        e.depends = Some(dependencies);
        Ok(())
    }

    fn verify(&mut self) -> Result<(), ParseError>{
        for i in 0..(self.data.len()) {
            let pos = self.data[i].0;
            if let Eq(_) = self.data[i].1 {
                self.populate_dependencies(pos, pos)?;
            }
        }
        Ok(())
    }
}

fn debug_parse_position(s: &str) -> Result<Position, ParseError> {
    parse_position(s.chars().peekable().by_ref(), Position::new(0,0))
}

fn parse_position(input: &mut iter::Peekable<impl Iterator<Item=char>>, whither: Position) -> Result<Position, ParseError> {
    // From_fn from: https://www.reddit.com/r/rust/comments/f8ae6q/comment/jwuyzgo
    let col_part = iter::from_fn(|| input.next_if(|c| c.is_ascii_alphabetic())).collect::<Vec<_>>();

    let mut col_number: usize = 0;
    let mut cols_parsed = false;
    for (place, ch) in col_part.iter().rev().enumerate() {
        cols_parsed = true;
        let ch = ch.to_ascii_lowercase();
        let digit = ch.to_digit(36).unwrap() - 10 + 1; // Subtract the actual digits out, then make zero-positioned
        col_number += 26usize.pow(place as u32) * (digit as usize);
    }

    if !cols_parsed {
        return Err(ParseError::InvalidEquation(whither))
    }

    let s = iter::from_fn(|| input.next_if(|c| c.is_ascii_digit())).collect::<String>();

    let row_number = s.parse::<usize>().map_err(|_| ParseError::InvalidEquation(whither))?;

    if row_number == 0 {
        return Err(ParseError::InvalidEquation(whither))
    }

    Ok(Position {
        col: col_number - 1,
        row: row_number - 1
    })
}

fn parse_equation(input: &str, whither: Position) -> Result<Equation, ParseError> {
    let mut chars = input.chars().peekable();

    // skip '='
    assert!(chars.next() == Some('='));

    let pos = parse_position(chars.by_ref(), whither)?;

    if chars.next().is_some() {
        Err(ParseError::InvalidEquation(whither))
    } else {
        Ok(Equation::new(EquationTree::Equals(pos)))
    }
}

fn parse_cell(input: &str, whither: Position) -> Result<Cell, ParseError> {
    // by this point input should be trimmed
    if input.is_empty() {
        return Ok(Cell::Nothing)
    }
    if let Ok(num) = input.parse() {
        Ok(Cell::Num(num))
    } else if input.chars().next() == Some('=') {
        let eq = parse_equation(input, whither)?;
        Ok(Cell::Eq(eq))
    } else {
        Ok(Cell::Text(input.to_string()))
    }
}

fn main() {
    // let msg = " 1 | | 3 \n A1 | =A1 | 1 \n 3333 | 2kmb | 1";

    let msg = " =B1 | =A1 | =D1 | =E1 | 2";

    let mut sheet = Sheet::from_str(msg);
    println!("--------------------");
    println!("{sheet}");
    println!("--------------------");

    if let Err(e) = sheet.verify() {
        println!("{e}");
    }

    sheet.solve().unwrap();

    println!("{sheet}");
    println!("--------------------");

    // let t = parse_equation("=AA31");
    // println!("{t:?}")
}
