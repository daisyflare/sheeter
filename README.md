# Sheeter

A very simple one-step spreadsheet solver, written in Rust.

This is based off a coding prompt that I heard somewhere. It does not actually render the spreadsheet in a graphical application like, say, excel, but it does verify that the sheet can be solved and then solve it, to be printed in terminal. 

# IN PROGRESS

Sheeter is a work in progress!!!! Currently, the sheet being rendered is hard-baked into the code. Also, the only equations allowed right now are just "=A1" type things. These will be fixed in the future.

# Using

``` sh
$ git clone https://gitlab.com/daisyflare/sheeter
$ cd sheeter
$ cargo run
```

Requires [rust](https://www.rustup.rs) to run.
